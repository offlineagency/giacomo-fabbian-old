$(document).ready(function() {


	$(".modal-send").click(function( event ) {
		event.preventDefault();
    //alert("cuai");
    $( ".output-message" ).html('').removeClass('some-error');
    var error = false;
    //$("#modal1").data('status','pending');
    var status = $("#modal1").data('status');


    var nome = $('#modal-nome').val();
    var cognome =  "";
    var email =  $('#modal-email').val();    
    var oggetto =  getOggetto();
    var messaggio =  $('#modal-messaggio').val();
    var azienda =  $('#modal-azienda').val();
    var budget =  $('#modal1 .value.min').html();
    var consenso = "";

    var telefono =  $('#modal-telefono').val(); 
    telefono = telefono.toString();

    if($('#modal-consenso').is(':checked'))
    	consenso = "on";

    
    var idform = "contatti"
    //alert("nome "+nome+" cognome"+cognome+" email"+email+" telefono"+telefono+" oggetto"+oggetto+" messaggio"+messaggio+" azienda"+azienda+" budget"+budget+" consenso"+consenso);
    //alert("email:"+email);

    if(nome == ""){
    	$( "#modal-nome" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {$( "#modal-nome" ).addClass('valid').removeClass('invalid');}
    
    if( consenso == "on"){
    	$('.modal-privacy').addClass('consenso-valid').removeClass('consenso-invalid');
    	
    }
    else {
    	$('.modal-privacy').addClass('consenso-invalid').removeClass('consenso-valid');
    	error = true;
    }
    if( !validateEmail(email)){
    	$( "#modal-email" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {
    	$( "#modal-email" ).addClass('valid').removeClass('invalid');
    }

    if(!validateTel(telefono)){
    	$( "#modal-telefono" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {
    	$( "#modal-telefono" ).addClass('valid').removeClass('invalid');
    }        

    /*if( oggetto == ""){
    	$( ".no-object" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {$( ".no-object" ).addClass('valid').removeClass('invalid');}*/      
    if( messaggio == "" || messaggio == undefined){
    	$("#modal-messaggio" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {$("#modal-messaggio" ).addClass('valid').removeClass('invalid');}      
    /*if( captha == ""){
    	$( ".no-captcha" ).addClass('invalid').removeClass('valid');
    	error = true;
    }
    else {
    	$( ".no-captcha" ).addClass('valid').removeClass('invalid');
    } */     

    

    

    if(error){
    	$( ".output-message" ).html('Uno o più campi hanno degli errori. Per favore controlla e prova di nuovo.').addClass('some-error').addClass('invalid').removeClass('valid');	
    }
    else if(status != "pending"){
    	$.ajax({
    		type: "POST",
    		url: window.location.protocol+"//"+window.location.host+"/gestione_utenti/form/elabora.php",
    		data: "nome=" + nome + "&cognome=" + cognome+ "&email=" +email+ "&telefono=" +telefono+ "&oggetto=" +oggetto+ "&messaggio=" + messaggio +"&idform=" +idform + "&azienda=" + azienda+"&budget="+budget,
    		dataType: "html",
    		
    		beforeSend: function () {
    			$(".ajax-loader").show();
    			$("#modal1").data('status','pending');
    		},

    		error: function(){
    			$(".ajax-loader").hide();
    			$("#modal1").data('status','');
    			$(".output-message").addClass('some-error').html('Errore no Internet, controlla lo stato della tua connessione e riprova.').fadeIn('slow').delay(3000).fadeOut('slow').html().removeClass('some-error');
    			$('#modal1 input').val('');
    		},

    		success: function(r){
    			$(".ajax-loader").hide();

    			var response = JSON.parse(r);   
    			$(".output-message").removeClass('some-error').html(response.txt).addClass(response.myclass).fadeIn('slow').delay(3000).fadeOut('slow').html().removeClass(response.myclass);
    			$('#modal1 input').val('');
    			$("#modal1").data('status','');


    		}
    	});
    }
    else {
    	$( ".output-message" ).html("Attendi, c'è già una rischiesta in corso.").addClass('some-error').addClass('invalid').removeClass('valid');
    }
});
function validateEmail(email) {
	if(email != ""){
		var emailReg = /^([\w-\.]+@([\w-]+\.)+[\w-]{2,4})?$/;
		return emailReg.test(email);
	}
	else{
		return false;
	}

}

function validateTel(telefono) {
	if(telefono != ""){
		var intRegex = /[0-9 -()+]+$/;
		return intRegex.test(telefono);
	}
	else{
		return false;
	}
}

function getOggetto(){
	var oggetto = "*";
	if($('#modal_sito').is(':checked')){
		oggetto += "sito*";
	}
	if($('#modal_app').is(':checked')){
		oggetto += "app*";
	}
	if($('#modal_ecommerce').is(':checked')){
		oggetto += "ecommerce*";
	}
	if($('#modal_altro').is(':checked')){
		oggetto += "altro*";
	}
	if(oggetto != "*")
		return oggetto;
	else{
		return "";
	}
}



});







