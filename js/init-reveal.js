window.sr = ScrollReveal();
sr.reveal('.skills, .pj, .chisono, #timeline, .loader, .skill-title, .skill-image, .timeline-panel, .timeline-badge, .go-up', { duration: 1200, afterReveal : function(domEl) {

 var hasClassLoader = $(domEl).hasClass("loader");
 var hasClassTitle = $(domEl).hasClass("skill-title");
 
 

 if (hasClassLoader){
  var bar = $(domEl).children();
  var dat = $(bar).data("perc");
  var val = dat+"%";


  
          //alert("val: "+val);
          $(bar).animate({
            width:val
          });

          $(domEl).removeClass("loader");
        }

        else if (hasClassTitle){
          var bar = $(domEl).parent().find('.determinate');
          var dat = $(bar).data("perc");

          $('<div class="skill-sub"><div class="skill-number"></div><div class="skill-indicator"></div></div>').insertAfter($(domEl));

          var sub = $(domEl).next();
          var num = $(sub).find('.skill-number');
          var indicator = $(sub).find('.skill-indicator');

      //fix
      var screenwidth = $(document).width();
      var fix = 0;
      if (screenwidth >1024){
        fix = 3.6;
      }
      else if(screenwidth <= 1024 && screenwidth > 768){
        fix = 6.5;
      }
      else if(screenwidth <= 767){
        fix = 5.5;
      }
      var val1 = (parseInt(dat) - fix)+ "%";
      var val2 = (parseInt(dat)- 0.15)+ "%";
      dat = dat + "%";

      //alert(dat + "-" +val2)

      $(num).css("left", val1).html(dat);
      $(indicator).css("left", val2);
      $(sub).delay(1000).fadeIn('slow');


    }


  }});